class lists(object):

    def __init__(self, alist):
        self.alist = alist

    def bubblesort(self):
        a = self.alist
        n = len(a)
        for i in range(n-1):
            for flag in range(n-1-i):
                if a[flag] > a[flag+1]:
                    a[flag], a[flag+1] = a[flag+1], a[flag]
        print(a)

    def selection_sort(self):
        a = self.alist
        n = len(a)
        for i in range(n-1):
            current = i
            for flag in range(i,n):
                if a[flag] <= a[current]:
                    current = flag
            a[i], a[current] = a[current], a[i]
        print(a)

    def insertion_sort(self):
        a = self.alist
        n = len(a)
        for i in range(1, n):
            clone = a[i]
            for flag in range(i, -1, -1):
                if clone < a[flag-1]:
                    a[flag] = a[flag-1]
                else:
                    break
            a[flag] = clone
        print(a)
        

    def merge_sort(self):
        n = self.alist

        def merge(n):
            merged = []
            if len(n) < 2:
                return n
            mid = len(n)//2

            # Recur before n becomes a list of single element
            a = merge(n[:mid])
            b = merge(n[mid:])

            while len(a) > 0 or len(b) > 0:
                try:
                    if a[0] <= b[0]:
                        merged.append(a[0])
                        a.pop(0)
                    else:
                        merged.append(b[0])
                        b.pop(0)
                except IndexError:
                    if len(a) > 0:
                        merged.extend(a)
                        a = []
                    if len(b) > 0:
                        merged.extend(b)
                        b = []
                    else:
                        pass
            return merged

        print(merge(n))


testlist = [3, 1, 4, 1, 5, 9, 2, 6]
testlist = lists(testlist)

testlist.merge_sort()
