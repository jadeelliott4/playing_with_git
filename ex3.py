#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Sep 28 20:17:02 2018

@author: elliotj
"""

x = 1
y = 2
sum = 0

while x < 4000000:
    next_term = x + y 
    x = y
    y = next_term
    if x%2 == 0:
        sum = sum + x
print (sum)