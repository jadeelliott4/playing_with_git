#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Sep 26 20:34:20 2018

@author: elliotj
"""
import math

number_x = int(input("Enter number x: "))
number_y = int(input("Enter number y: "))
number_z = number_x ** number_y
log_x = math.log2(number_x)
print(f"x**y = {number_z}")
print(f"log({number_x}) = {log_x}")