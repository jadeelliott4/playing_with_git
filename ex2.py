#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep 27 20:05:19 2018

@author: elliotj
"""
sum = 0
for i in range(0, 1001):
    if i%3 == 0 or i%5 == 0:
        sum = sum + i
        
print (sum)