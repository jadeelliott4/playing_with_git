import random 

def monty():
    counter = 0;
    for j in range(1000):
        available_numbers = [1,2,3]
        door_with_car = random.randint(1,3)
        first_door_choice = random.randint(1,3)
        
        i = available_numbers.index(first_door_choice)
        a = available_numbers.index(door_with_car)
        
        doors_not_picked = available_numbers[:i] + available_numbers[i+1 :] 
        doors_with_no_car = available_numbers[:a] + available_numbers[a+1 :] 
        
        opened_door = random.choice(list(set(doors_not_picked).intersection(doors_with_no_car)))
        
        available_numbers.remove(opened_door)
        available_numbers.remove(first_door_choice)
        swapped_door = available_numbers[0]
        
        if swapped_door == door_with_car:
            counter += 1
    print(counter)